#include <stdio.h>
#include <stdlib.h>

int main()
{   //variables
    int number1,number2;

    //get user inputs
    printf("Enter your first number  :");
    scanf("%d",&number1);

    printf("Enter your second number :");
    scanf("%d",&number2);

    printf("before swapping= %d %d ",number1,number2 );

    number1 = number1 - number2;
    number2 = number1 + number2;
    number1 = number2 - number1;

    printf("\nafter swapping = %d %d ",number1 ,number2);


    return 0;
}

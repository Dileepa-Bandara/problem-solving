#include <stdio.h>
#include <stdlib.h>

int main()
{   int A = 10,B = 15 ;
    printf("Output = %d \n",A&B);
    printf("\n");
    printf("Output = %d \n",A^B);
    printf("\n");
    printf("Output = %d\n",~A);

    printf("\n");

    int i;
    for (i=0; i<=3; ++i)
        printf("Left shift by %d: %d\n", i, A<<i);

    printf("\n");

    for (i=0; i<=3; ++i)
        printf("Right shift by %d: %d\n", i, B>>i);
    return 0;
}

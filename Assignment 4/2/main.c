#include <stdio.h>
#include <stdlib.h>

int main()
{
    //variables
    float height,radius;
    //get user inputs
    printf("Enter the height of the cone   :");
    scanf("%f",&height);

    printf("Enter the radius of the cone   :");
    scanf("%f",&radius);

    //formula
    float pie=3.14285714286;
    float volume=pie*radius*radius*height/3;

    //volume of the cone
    printf("Volume of the cone=%.2f",volume);
    return 0;
}

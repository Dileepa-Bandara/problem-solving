#include <stdio.h>
#include <stdlib.h>

int main()
{
    float celcius,fahrenheit;

    printf("\nEnter temperature in Celsius: ");
    scanf("%f",&celcius);
    fahrenheit= (celcius*1.8)+32;
    printf("Temperature in Fahrenheit: %.2f",fahrenheit);
    return 0;
}
